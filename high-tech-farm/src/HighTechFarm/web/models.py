from django.db import models
from versatileimagefield.fields import VersatileImageField


# class Moment(models.Model):
#     image = VersatileImageField('Photo',upload_to="web/moments/",blank=True,null=True)
#     date = models.DateField(blank=True, null=True)
#     title = models.CharField(max_length=128)

#     class Meta:
#         db_table = 'web_moments'
#         verbose_name = 'Moment'
#         verbose_name_plural = 'Moments'
#         ordering = ('date',)

#     def __str__(self):
#         return self.title


class Moment(models.Model):
    image = VersatileImageField('Photo',upload_to="web/gallery/",blank=True,null=True)
    date = models.DateField(blank=True, null=True)
    title = models.CharField(max_length=128)

    class Meta:
        db_table = 'web_gallery'
        verbose_name = 'Gallery'
        verbose_name_plural = 'Gallery'
        ordering = ('date',)

    def __str__(self):
        return self.title


class Contact(models.Model):
    name = models.CharField(max_length=128)
    email = models.EmailField()
    message = models.TextField()

    class Meta:
        db_table = 'web_contacts'
        verbose_name = 'Contact'
        verbose_name_plural = 'Contacts'
        ordering = ('name',)

    def __str__(self):
        return self.name


class ContactUs(models.Model):
    address = models.TextField()
    phone = models.CharField(max_length=128)
    email = models.EmailField()

    class Meta:
        db_table = 'web_contact_us'
        verbose_name = 'Contact Us'
        verbose_name_plural = 'Contact Us'

    def __str__(self):
        return self.phone

