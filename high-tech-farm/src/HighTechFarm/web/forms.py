from django import forms
from web.models import Contact
from django.forms.widgets import TextInput, Textarea, Select


class ContactForm(forms.ModelForm):

    class Meta:
        model = Contact
        fields = '__all__'
        widgets = {
            'name': TextInput(attrs={'class':'required','placeholder':'Type Your Name'}),
            'email': TextInput(attrs={'class':'required','placeholder':'Type Your Email'}),
            'message': Textarea(attrs={'placeholder':'Type Your messages'}),
        }
        error_messages = {
            'name':{
                'required': ("Name field is required"),
            },
            'email':{
                'required': ("Email field is required"),
            }
        }
