from django.db import models
from versatileimagefield.fields import VersatileImageField
import uuid


class Product(models.Model):
	id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	image = VersatileImageField('Photo',upload_to="web/products/",blank=True,null=True)
	age = models.CharField(max_length=10)
	name = models.CharField(max_length=128)
	price = models.DecimalField(max_digits=8)
