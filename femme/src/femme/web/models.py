from django.db import models

class Registration(models.Model):
    name = models.CharField(max_length = 120)
    email = models.EmailField()
    phone = models.CharField(max_length = 120)
    education = models.CharField(max_length = 120)
    dob = models.DateField()
    message = models.TextField()

    class Meta:
        db_table = 'web_registration'
        verbose_name = 'registration'
        verbose_name_plural = 'registrations'

    def __str__(self):
        return self.name


class About(models.Model):
    title = models.CharField(max_length=120)
    content = models.TextField()
    image = models.ImageField(upload_to="web/about")

    class Meta:
        db_table = 'web_about'
        verbose_name = 'about'
        verbose_name_plural = 'about'

    def __str__(self):
        return self.titile