# Generated by Django 3.1.2 on 2020-10-03 04:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titile', models.CharField(max_length=120)),
                ('content', models.TextField()),
                ('image', models.ImageField(upload_to='web/about')),
            ],
            options={
                'verbose_name': 'about',
                'verbose_name_plural': 'about',
                'db_table': 'web_about',
            },
        ),
    ]
