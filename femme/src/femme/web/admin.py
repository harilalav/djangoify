from django.contrib import admin
from web.models import Registration, About


class RegistrationAdmin(admin.ModelAdmin):
    list_display = ('id','name','email','phone','dob','education','message')
    search_fields = ('name',)

admin.site.register(Registration, RegistrationAdmin)

class AboutAdmin(admin.ModelAdmin):
    list_display = ('title','content','image')
    search_fields = ('title',)

admin.site.register(About, AboutAdmin)

