from django.shortcuts import render
from web.models import Registration,About
from django.http import HttpResponse
import json
from django.urls import reverse
from web.forms import RegistrationForm
from web.functions import generate_form_errors


def index(request):
    about = About.objects.all()
    registration = Registration.objects.all()
    form = RegistrationForm()
    context = {
        "title" : "Home",
        "about" : about,
        "registrations" : registration,
        "form" : form,
    }
    return render(request, "index.html", context)


def about(request):
    context = {
        "title" : "About",
        "about" : about,
    }
    return render(request, "about.html", context)

def registration(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()

            response_data = {
                'status' : 'true',
                'title' : 'succesfully submitted',
                'message' : 'registration completed succesfully',
            }
        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : 'form validation error',
                'message' : message,
        }

            return HttpResponse(json.dumps(response_data),content_type = 'application/javascript')
    else:
        return HttpResponse("Access Denied")