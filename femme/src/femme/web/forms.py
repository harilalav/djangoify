from django import forms
from django.forms.widgets import Textarea, TextInput
from web.models import Registration


class RegistrationForm(forms.ModelForm):

    class Meta:
        model = Registration
        fields = "__all__"
        widgets = {
            'name' : TextInput(attrs = {'class' : 'required form-control', 'placeholder' : 'Name'}),
            'email' : TextInput(attrs = {'class' : 'required form-control', 'placeholder' : 'Email'}),
            'phone' : TextInput(attrs = {'class' : 'required form-control', 'placeholder' : 'Education'}),
            'dob' : TextInput(attrs = {'class' : 'required form-control', 'placeholder' : 'DOB'}),
            'message' : TextInput(attrs = {'class' : 'required form-control', 'placeholder' : 'Message'}),
        }
        error_messages = {
            'name' : {
                'required' : ("Name field is required."),
            },
            'phone' : {
                'required' : ("Name field is required."),
            },
            'email' : {
                'required' : ("Email field is required."),
            },
            'dob' : {
                'required' : ("DOB field is required."),
            },
            'education' : {
                'required' : ("Education field is required."),
            },
            'message' : {
                'required' : ("Message field is required."),
            },
        }
        labels = {
            "email" : "E-Mail",
            "message" : ""
        }
        