from django.urls import path, re_path
from web import views


app_name = "web"

urlpatterns = [
    path('', views.index),
    path('about/', views.about),
    re_path(r'^registration/$', views.registration, name = "registration")
    
]
