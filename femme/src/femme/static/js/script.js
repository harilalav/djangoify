

function resize(){
	
	function equalservices(){
        var h = 0;
        $('#services .services ul li').outerHeight('auto');
        $('#services .services ul li').each(function(){
            var height = $(this).outerHeight();
            if(height > h){
                h = height;
            }
        });
        $('#services .services ul li').outerHeight(h);
    }
    equalservices();

    function changeContentHeight1(){
        var aHeight = $('#about .left').outerHeight();
        $('#about .right').css({
            "height" : aHeight
        });
    }
    
    var window_width = $(window).width();
    if (window_width < 1281) {
        changeContentHeight1();
    }

}
function show_loader(){
	$('body').append('<div class="popup-box"><div class="preloader pl-xxl"><svg viewBox="25 25 50 50" class="pl-circular"><circle r="20" cy="50" cx="50" class="plc-path"/></svg></div></div><span class="popup-bg"></span>');
}

function remove_popup(){
	$('.popup-box,.popup-bg').remove();
}


$(document).ready(function(){  
	$('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	        if (target.length) {
	            $('html, body').animate({
	              scrollTop: target.offset().top
	            }, 2000);
	            return true;
	        }
        }
    });
    
    $('ul#accordion li h4 span').click(function(){
        var $this = $(this);
        $this.parents('li').toggleClass('active');
        $this.parents('li').find('div.content').slideToggle();
        $this.parents('li').find('h4').toggleClass('active');
    });
    $('ul#accordion li.active div.content').show();

    resize();
});

$(window).load(function(){
	resize();
});

$(window).resize(function(){
	resize();
});
